grammar lua;

@header{
package lua;

import java.util.List;
import java.util.LinkedList;
import lua.ast.*;
}

@lexer::header {
package lua;
}

prog returns [List<Statement> result]
	@init {
	$result = new LinkedList<>();
	}
	:	(s=sttm	{if ($s.result != null) $result.add($s.result);})+
	;
	
sttm returns [Statement result]
	:	a=assign ';'	{$result = $a.result;}
	|	e=expr ';' 	{$result = $e.result;}
	|	i=if_sttm	{$result = $i.result;}
	|	w=while_sttm 	{$result = $w.result;}
	|	f=for_sttm	{$result = $f.result;}
	|	b=block_sttm 	{$result = $b.result;}
	|	v=function_expr {$result = $v.result;}
	;
	
assign returns [Statement result]
	:	ID '=' e=expr	
		{
		$result = new Assign($ID.text, $e.result);
		}			
	;	
	
assign_temp returns [Statement result]
	:	ID '=' e=expr	
		{
		$result = new AssignTemp($ID.text, $e.result);
		}			
	;	
	
if_sttm	returns [Statement result]
	:	IF e=expr THEN s1=sttm (ELSE s2=sttm)? 
		{
		$result = new IfSttm($e.result, $s1.result, $s2.result);
		}	
		END	
	;
	
while_sttm returns [Statement result]
	:	WHILE e=expr s=block_sttm
		{
		$result = new WhileSttm($e.result, $s.result);
		}
	;
	
for_sttm returns [Statement result]
	:	FOR e=assign_temp ',' l=base_expr ',' i=base_expr s=block_sttm_for
		{
		$result = new ForSttm($e.result,$l.result,$i.result,$s.result);
		}	
	;
	

	
block_sttm returns [Statement result]
	@init {
	List<Statement> sttmList = new LinkedList<>();
	}
	:	DO
		(s=sttm {sttmList.add($s.result);})*
		END
		{$result = new BlockSttm(sttmList);}
	;
	
block_sttm_function returns [BlockSttm result]
	@init {	
	List<Statement> sttmList = new LinkedList<>();
	}
	:	
		(s=sttm {sttmList.add($s.result);})*
		(RETURN e=expr ';' {sttmList.add($e.result);})?
		END
		{$result = new BlockSttm(sttmList);}
	;	

block_sttm_for returns [Statement result]
	@init {
	List<Statement> sttmList = new LinkedList<>();
	}
	:	DO
		(s=sttm {sttmList.add($s.result);})*
		END
		{$result = new BlockSttmFor(sttmList);}
	;
	
expr returns [Expr result]
	:	e=and_expr	{$result = $e.result;}
	
	;
	
and_expr returns [Expr result]
	:	e1=or_expr 	{$result = $e1.result;}
		(AND e2=or_expr	
		{
		$result = new CompositeExpr(Ops.AND, $result, $e2.result);
		}
		)*
	;
	
or_expr	returns [Expr result]
	:	e1=rel_expr 	{$result = $e1.result;}
		(OR e2=rel_expr
		{
		$result = new CompositeExpr(Ops.OR, $result, $e2.result);
		}
		)*
	;

rel_expr returns [Expr result]
	:	e1=add_expr 	{$result = $e1.result;}
		(o=rel_op e2=add_expr
		{
		$result = new CompositeExpr($o.op, $result, $e2.result);
		}
		)?
	;
	
rel_op returns [Operator op]
	:	EQ	{$op = Ops.EQU;}
	|	NEQ	{$op = Ops.NEQ;}
	|	GT	{$op = Ops.GT;}
	|	GEQ	{$op = Ops.GEQ;}
	|	LT	{$op = Ops.LT;}
	|	LEQ	{$op = Ops.LEQ;}
	;
	
add_expr returns [Expr result]
	:	e1=sub_expr 	{$result = $e1.result;}
		('+' e2=sub_expr {$result = new CompositeExpr(Ops.PLUS, $result, $e2.result);}		
		)*
	;
	
sub_expr returns [Expr result]
	:	e1=mult_expr 	{$result = $e1.result;}
		('-' e2=mult_expr {$result = new CompositeExpr(Ops.MINUS, $result, $e2.result);}
		)*
	;

mult_expr returns [Expr result]
	:	e1=qut_expr 	{$result = $e1.result;}
		('*' e2= qut_expr {$result = new CompositeExpr(Ops.MULT, $result, $e2.result);})*
	;
qut_expr returns [Expr result]
	:	e2=div_expr 	{$result = $e2.result;}
		('/' e2= div_expr {$result = new CompositeExpr(Ops.DIV, $result, $e2.result);})*
	;
	
div_expr returns [Expr result]
	:	e2=mod_expr 	{$result = $e2.result;}
		(DIV e2= mod_expr {$result = new CompositeExpr(Ops.IDIV, $result, $e2.result);})*
	;
mod_expr returns [Expr result]
	:	e2=expt_expr 	{$result = $e2.result;}
		(MOD e2= expt_expr {$result = new CompositeExpr(Ops.MOD, $result, $e2.result);})*
	;
	
expt_expr returns [Expr result]
	:	e1=base_expr 	{$result = $e1.result;}
		('^' e2=base_expr
		{
		$result = new CompositeExpr(Ops.EXPT, $result, $e2.result);
		}
		)*
	;
		
base_expr returns [Expr result]
	:	INT		{$result = new NumericExpr($INT.text);}
	|	FLOAT		{$result = new NumericExpr($FLOAT.text);}
	| 	STRING		{$result = new StringExpr($STRING.text);}
	|	ID		{$result = new VarExpr($ID.text);}
	|	f=func_call		{$result = $f.result;}
	|	'-' e1=base_expr	{$result = new CompositeExpr(Ops.UMINUS, $e1.result);}
	|	NOT e1=base_expr	{$result = new CompositeExpr(Ops.NOT, $e1.result);}
	|	'(' e1=expr ')'		{$result = $e1.result;}
	;
	
function_expr returns [Function result]
	:
		FUNCTION ID '(' args=arg_list_text? ')' b=block_sttm_function
		{
			$result = new Function($ID.text, $args.list, $b.result);
		}
	;
	
function_call returns [FunctionCall result]
	:
		FUNCTION ID '(' args=arg_list? ')' ';'
		{ 
			$result = new FunctionCall($ID.text, $args.list);
		}
	;
	
func_call returns [Expr result]
	:	ID '(' args=arg_list? ')' 
		{		
		Operator fn = Func.find($ID.text);
		List<Expr> argList = $args.list;
		if (fn != null){
			$result = new CompositeExpr(fn, argList);
		}else{
			$result = new FunctionCall($ID.text, argList);
		}
		}
	;

string_expr returns [String result]
	:
		STRING {$result = $STRING.text;}
	;
	
arg_list returns [List<Expr> list]
	@init {
	$list = new LinkedList<Expr>();
	}
	:	e1=expr 	{$list.add($e1.result);}
		(',' e2=expr	{$list.add($e2.result);}
		)*
	;
	
arg_list_text returns [List<String> list]
	@init {
	$list = new LinkedList<String>();
	}
	:	e1= ID 	{$list.add($e1.text);}
		(',' e2= ID	{$list.add($e2.text);}
		)*
	;

FUNCTION 
	:	'function';
RETURN	:	'return';
IF	:	'if';
THEN	:	'then';
ELSE	:	'else';
WHILE	:	'while';
FOR	:	'for';
DO	:	'do';
END	:	'end';
DIV	:	'div';
MOD	:	'%';
EQ	:	'==';
NEQ	:	'!=';
GT	:	'>';
GEQ	:	'>=';
LT	:	'<';
LEQ	:	'<=';
AND	:	'and';
OR	:	'or';
NOT	:	'not';

ID  :	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

INT :	'0'..'9'+
    ;

FLOAT
    :   ('0'..'9')+ '.' ('0'..'9')* EXPONENT?
    |   '.' ('0'..'9')+ EXPONENT?
    |   ('0'..'9')+ EXPONENT
    ;
    
STRING 
    :  '"' (~('\\'|'"') )* '"'    	
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

WS  :   ( ' '	
	| '\r'
        | '\n'
        | '\t'
        ) {$channel=HIDDEN;}
    ;
    
//NL	:	'\r'
//        | 	'\n'
//        ;
    
fragment
EXPONENT : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;
