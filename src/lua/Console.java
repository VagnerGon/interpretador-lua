/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua;

import java.io.File;
import java.io.FileInputStream;
import lua.ast.Statement;
import lua.ast.Context;
import org.antlr.runtime.*;
import java.util.List;

/**
 *
 * @author 1208144
 */
public class Console {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Throwable {
        //ANTLRInputStream input = new ANTLRInputStream(System.in);
        ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(new File("C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\entrada.txt")));
        luaLexer lexer = new luaLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        luaParser parser = new luaParser(tokens);
        
        // Processa a entrada e gera o AST.
        List<Statement> sttmList = parser.prog();
        /*        // Imprime o resultado do “parsing”
         * System.out.println("AST lido:");
         * for (Statement s: sttmList) {
         * String ln = s.toString();
         * System.out.println(ln);
         * }
         * System.out.print("\n\n");*/
        
        // Processa o programa e gera o resultado.        
        Context ctx = new Context();
        
        for (Statement s: sttmList) {
            s.exec(ctx);
            //System.out.println(s.toString());
        }        
    }
}
