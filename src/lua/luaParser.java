// $ANTLR 3.5 C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g 2013-04-19 17:03:18

package lua;

import java.util.List;
import java.util.LinkedList;
import lua.ast.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class luaParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "COMMENT", "DIV", "DO", 
		"ELSE", "END", "EQ", "EXPONENT", "FLOAT", "FOR", "FUNCTION", "GEQ", "GT", 
		"ID", "IF", "INT", "LEQ", "LT", "MOD", "NEQ", "NOT", "OR", "RETURN", "STRING", 
		"THEN", "WHILE", "WS", "'('", "')'", "'*'", "'+'", "','", "'-'", "'/'", 
		"';'", "'='", "'^'"
	};
	public static final int EOF=-1;
	public static final int T__31=31;
	public static final int T__32=32;
	public static final int T__33=33;
	public static final int T__34=34;
	public static final int T__35=35;
	public static final int T__36=36;
	public static final int T__37=37;
	public static final int T__38=38;
	public static final int T__39=39;
	public static final int T__40=40;
	public static final int AND=4;
	public static final int COMMENT=5;
	public static final int DIV=6;
	public static final int DO=7;
	public static final int ELSE=8;
	public static final int END=9;
	public static final int EQ=10;
	public static final int EXPONENT=11;
	public static final int FLOAT=12;
	public static final int FOR=13;
	public static final int FUNCTION=14;
	public static final int GEQ=15;
	public static final int GT=16;
	public static final int ID=17;
	public static final int IF=18;
	public static final int INT=19;
	public static final int LEQ=20;
	public static final int LT=21;
	public static final int MOD=22;
	public static final int NEQ=23;
	public static final int NOT=24;
	public static final int OR=25;
	public static final int RETURN=26;
	public static final int STRING=27;
	public static final int THEN=28;
	public static final int WHILE=29;
	public static final int WS=30;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public luaParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public luaParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return luaParser.tokenNames; }
	@Override public String getGrammarFileName() { return "C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g"; }



	// $ANTLR start "prog"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:15:1: prog returns [List<Statement> result] : (s= sttm )+ ;
	public final List<Statement> prog() throws RecognitionException  {
		List<Statement> result = null;


		Statement s =null;


			result = new LinkedList<>();
			
		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:19:2: ( (s= sttm )+ )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:19:4: (s= sttm )+
			{
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:19:4: (s= sttm )+
			int cnt1=0;
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0==DO||(LA1_0 >= FLOAT && LA1_0 <= FUNCTION)||(LA1_0 >= ID && LA1_0 <= INT)||LA1_0==NOT||LA1_0==STRING||LA1_0==WHILE||LA1_0==31||LA1_0==36) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:19:5: s= sttm
					{
					pushFollow(FOLLOW_sttm_in_prog38);
					s=sttm();
					state._fsp--;

					if (s != null) result.add(s);
					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					EarlyExitException eee = new EarlyExitException(1, input);
					throw eee;
				}
				cnt1++;
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "prog"



	// $ANTLR start "sttm"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:22:1: sttm returns [Statement result] : (a= assign ';' |e= expr ';' |i= if_sttm |w= while_sttm |f= for_sttm |b= block_sttm |v= function_expr );
	public final Statement sttm() throws RecognitionException  {
		Statement result = null;


		Statement a =null;
		Expr e =null;
		Statement i =null;
		Statement w =null;
		Statement f =null;
		Statement b =null;
		Function v =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:23:2: (a= assign ';' |e= expr ';' |i= if_sttm |w= while_sttm |f= for_sttm |b= block_sttm |v= function_expr )
			int alt2=7;
			switch ( input.LA(1) ) {
			case ID:
				{
				int LA2_1 = input.LA(2);
				if ( (LA2_1==39) ) {
					alt2=1;
				}
				else if ( (LA2_1==AND||LA2_1==DIV||LA2_1==EQ||(LA2_1 >= GEQ && LA2_1 <= GT)||(LA2_1 >= LEQ && LA2_1 <= NEQ)||LA2_1==OR||LA2_1==31||(LA2_1 >= 33 && LA2_1 <= 34)||(LA2_1 >= 36 && LA2_1 <= 38)||LA2_1==40) ) {
					alt2=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 2, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case FLOAT:
			case INT:
			case NOT:
			case STRING:
			case 31:
			case 36:
				{
				alt2=2;
				}
				break;
			case IF:
				{
				alt2=3;
				}
				break;
			case WHILE:
				{
				alt2=4;
				}
				break;
			case FOR:
				{
				alt2=5;
				}
				break;
			case DO:
				{
				alt2=6;
				}
				break;
			case FUNCTION:
				{
				alt2=7;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}
			switch (alt2) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:23:4: a= assign ';'
					{
					pushFollow(FOLLOW_assign_in_sttm60);
					a=assign();
					state._fsp--;

					match(input,38,FOLLOW_38_in_sttm62); 
					result = a;
					}
					break;
				case 2 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:24:4: e= expr ';'
					{
					pushFollow(FOLLOW_expr_in_sttm71);
					e=expr();
					state._fsp--;

					match(input,38,FOLLOW_38_in_sttm73); 
					result = e;
					}
					break;
				case 3 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:25:4: i= if_sttm
					{
					pushFollow(FOLLOW_if_sttm_in_sttm83);
					i=if_sttm();
					state._fsp--;

					result = i;
					}
					break;
				case 4 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:26:4: w= while_sttm
					{
					pushFollow(FOLLOW_while_sttm_in_sttm92);
					w=while_sttm();
					state._fsp--;

					result = w;
					}
					break;
				case 5 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:27:4: f= for_sttm
					{
					pushFollow(FOLLOW_for_sttm_in_sttm102);
					f=for_sttm();
					state._fsp--;

					result = f;
					}
					break;
				case 6 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:28:4: b= block_sttm
					{
					pushFollow(FOLLOW_block_sttm_in_sttm111);
					b=block_sttm();
					state._fsp--;

					result = b;
					}
					break;
				case 7 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:29:4: v= function_expr
					{
					pushFollow(FOLLOW_function_expr_in_sttm121);
					v=function_expr();
					state._fsp--;

					result = v;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "sttm"



	// $ANTLR start "assign"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:32:1: assign returns [Statement result] : ID '=' e= expr ;
	public final Statement assign() throws RecognitionException  {
		Statement result = null;


		Token ID1=null;
		Expr e =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:33:2: ( ID '=' e= expr )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:33:4: ID '=' e= expr
			{
			ID1=(Token)match(input,ID,FOLLOW_ID_in_assign139); 
			match(input,39,FOLLOW_39_in_assign141); 
			pushFollow(FOLLOW_expr_in_assign145);
			e=expr();
			state._fsp--;


					result = new Assign((ID1!=null?ID1.getText():null), e);
					
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "assign"



	// $ANTLR start "assign_temp"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:39:1: assign_temp returns [Statement result] : ID '=' e= expr ;
	public final Statement assign_temp() throws RecognitionException  {
		Statement result = null;


		Token ID2=null;
		Expr e =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:40:2: ( ID '=' e= expr )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:40:4: ID '=' e= expr
			{
			ID2=(Token)match(input,ID,FOLLOW_ID_in_assign_temp170); 
			match(input,39,FOLLOW_39_in_assign_temp172); 
			pushFollow(FOLLOW_expr_in_assign_temp176);
			e=expr();
			state._fsp--;


					result = new AssignTemp((ID2!=null?ID2.getText():null), e);
					
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "assign_temp"



	// $ANTLR start "if_sttm"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:46:1: if_sttm returns [Statement result] : IF e= expr THEN s1= sttm ( ELSE s2= sttm )? END ;
	public final Statement if_sttm() throws RecognitionException  {
		Statement result = null;


		Expr e =null;
		Statement s1 =null;
		Statement s2 =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:47:2: ( IF e= expr THEN s1= sttm ( ELSE s2= sttm )? END )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:47:4: IF e= expr THEN s1= sttm ( ELSE s2= sttm )? END
			{
			match(input,IF,FOLLOW_IF_in_if_sttm201); 
			pushFollow(FOLLOW_expr_in_if_sttm205);
			e=expr();
			state._fsp--;

			match(input,THEN,FOLLOW_THEN_in_if_sttm207); 
			pushFollow(FOLLOW_sttm_in_if_sttm211);
			s1=sttm();
			state._fsp--;

			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:47:27: ( ELSE s2= sttm )?
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0==ELSE) ) {
				alt3=1;
			}
			switch (alt3) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:47:28: ELSE s2= sttm
					{
					match(input,ELSE,FOLLOW_ELSE_in_if_sttm214); 
					pushFollow(FOLLOW_sttm_in_if_sttm218);
					s2=sttm();
					state._fsp--;

					}
					break;

			}


					result = new IfSttm(e, s1, s2);
					
			match(input,END,FOLLOW_END_in_if_sttm230); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "if_sttm"



	// $ANTLR start "while_sttm"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:54:1: while_sttm returns [Statement result] : WHILE e= expr s= block_sttm ;
	public final Statement while_sttm() throws RecognitionException  {
		Statement result = null;


		Expr e =null;
		Statement s =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:55:2: ( WHILE e= expr s= block_sttm )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:55:4: WHILE e= expr s= block_sttm
			{
			match(input,WHILE,FOLLOW_WHILE_in_while_sttm247); 
			pushFollow(FOLLOW_expr_in_while_sttm251);
			e=expr();
			state._fsp--;

			pushFollow(FOLLOW_block_sttm_in_while_sttm255);
			s=block_sttm();
			state._fsp--;


					result = new WhileSttm(e, s);
					
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "while_sttm"



	// $ANTLR start "for_sttm"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:61:1: for_sttm returns [Statement result] : FOR e= assign_temp ',' l= base_expr ',' i= base_expr s= block_sttm_for ;
	public final Statement for_sttm() throws RecognitionException  {
		Statement result = null;


		Statement e =null;
		Expr l =null;
		Expr i =null;
		Statement s =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:62:2: ( FOR e= assign_temp ',' l= base_expr ',' i= base_expr s= block_sttm_for )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:62:4: FOR e= assign_temp ',' l= base_expr ',' i= base_expr s= block_sttm_for
			{
			match(input,FOR,FOLLOW_FOR_in_for_sttm275); 
			pushFollow(FOLLOW_assign_temp_in_for_sttm279);
			e=assign_temp();
			state._fsp--;

			match(input,35,FOLLOW_35_in_for_sttm281); 
			pushFollow(FOLLOW_base_expr_in_for_sttm285);
			l=base_expr();
			state._fsp--;

			match(input,35,FOLLOW_35_in_for_sttm287); 
			pushFollow(FOLLOW_base_expr_in_for_sttm291);
			i=base_expr();
			state._fsp--;

			pushFollow(FOLLOW_block_sttm_for_in_for_sttm295);
			s=block_sttm_for();
			state._fsp--;


					result = new ForSttm(e,l,i,s);
					
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "for_sttm"



	// $ANTLR start "block_sttm"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:70:1: block_sttm returns [Statement result] : DO (s= sttm )* END ;
	public final Statement block_sttm() throws RecognitionException  {
		Statement result = null;


		Statement s =null;


			List<Statement> sttmList = new LinkedList<>();
			
		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:74:2: ( DO (s= sttm )* END )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:74:4: DO (s= sttm )* END
			{
			match(input,DO,FOLLOW_DO_in_block_sttm325); 
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:75:3: (s= sttm )*
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( (LA4_0==DO||(LA4_0 >= FLOAT && LA4_0 <= FUNCTION)||(LA4_0 >= ID && LA4_0 <= INT)||LA4_0==NOT||LA4_0==STRING||LA4_0==WHILE||LA4_0==31||LA4_0==36) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:75:4: s= sttm
					{
					pushFollow(FOLLOW_sttm_in_block_sttm332);
					s=sttm();
					state._fsp--;

					sttmList.add(s);
					}
					break;

				default :
					break loop4;
				}
			}

			match(input,END,FOLLOW_END_in_block_sttm340); 
			result = new BlockSttm(sttmList);
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "block_sttm"



	// $ANTLR start "block_sttm_function"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:80:1: block_sttm_function returns [BlockSttm result] : (s= sttm )* ( RETURN e= expr ';' )? END ;
	public final BlockSttm block_sttm_function() throws RecognitionException  {
		BlockSttm result = null;


		Statement s =null;
		Expr e =null;

			
			List<Statement> sttmList = new LinkedList<>();
			
		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:84:2: ( (s= sttm )* ( RETURN e= expr ';' )? END )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:85:3: (s= sttm )* ( RETURN e= expr ';' )? END
			{
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:85:3: (s= sttm )*
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( (LA5_0==DO||(LA5_0 >= FLOAT && LA5_0 <= FUNCTION)||(LA5_0 >= ID && LA5_0 <= INT)||LA5_0==NOT||LA5_0==STRING||LA5_0==WHILE||LA5_0==31||LA5_0==36) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:85:4: s= sttm
					{
					pushFollow(FOLLOW_sttm_in_block_sttm_function372);
					s=sttm();
					state._fsp--;

					sttmList.add(s);
					}
					break;

				default :
					break loop5;
				}
			}

			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:86:3: ( RETURN e= expr ';' )?
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( (LA6_0==RETURN) ) {
				alt6=1;
			}
			switch (alt6) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:86:4: RETURN e= expr ';'
					{
					match(input,RETURN,FOLLOW_RETURN_in_block_sttm_function381); 
					pushFollow(FOLLOW_expr_in_block_sttm_function385);
					e=expr();
					state._fsp--;

					match(input,38,FOLLOW_38_in_block_sttm_function387); 
					sttmList.add(e);
					}
					break;

			}

			match(input,END,FOLLOW_END_in_block_sttm_function395); 
			result = new BlockSttm(sttmList);
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "block_sttm_function"



	// $ANTLR start "block_sttm_for"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:91:1: block_sttm_for returns [Statement result] : DO (s= sttm )* END ;
	public final Statement block_sttm_for() throws RecognitionException  {
		Statement result = null;


		Statement s =null;


			List<Statement> sttmList = new LinkedList<>();
			
		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:95:2: ( DO (s= sttm )* END )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:95:4: DO (s= sttm )* END
			{
			match(input,DO,FOLLOW_DO_in_block_sttm_for421); 
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:96:3: (s= sttm )*
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( (LA7_0==DO||(LA7_0 >= FLOAT && LA7_0 <= FUNCTION)||(LA7_0 >= ID && LA7_0 <= INT)||LA7_0==NOT||LA7_0==STRING||LA7_0==WHILE||LA7_0==31||LA7_0==36) ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:96:4: s= sttm
					{
					pushFollow(FOLLOW_sttm_in_block_sttm_for428);
					s=sttm();
					state._fsp--;

					sttmList.add(s);
					}
					break;

				default :
					break loop7;
				}
			}

			match(input,END,FOLLOW_END_in_block_sttm_for436); 
			result = new BlockSttmFor(sttmList);
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "block_sttm_for"



	// $ANTLR start "expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:101:1: expr returns [Expr result] : e= and_expr ;
	public final Expr expr() throws RecognitionException  {
		Expr result = null;


		Expr e =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:102:2: (e= and_expr )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:102:4: e= and_expr
			{
			pushFollow(FOLLOW_and_expr_in_expr458);
			e=and_expr();
			state._fsp--;

			result = e;
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "expr"



	// $ANTLR start "and_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:106:1: and_expr returns [Expr result] : e1= or_expr ( AND e2= or_expr )* ;
	public final Expr and_expr() throws RecognitionException  {
		Expr result = null;


		Expr e1 =null;
		Expr e2 =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:107:2: (e1= or_expr ( AND e2= or_expr )* )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:107:4: e1= or_expr ( AND e2= or_expr )*
			{
			pushFollow(FOLLOW_or_expr_in_and_expr480);
			e1=or_expr();
			state._fsp--;

			result = e1;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:108:3: ( AND e2= or_expr )*
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( (LA8_0==AND) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:108:4: AND e2= or_expr
					{
					match(input,AND,FOLLOW_AND_in_and_expr488); 
					pushFollow(FOLLOW_or_expr_in_and_expr492);
					e2=or_expr();
					state._fsp--;


							result = new CompositeExpr(Ops.AND, result, e2);
							
					}
					break;

				default :
					break loop8;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "and_expr"



	// $ANTLR start "or_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:115:1: or_expr returns [Expr result] : e1= rel_expr ( OR e2= rel_expr )* ;
	public final Expr or_expr() throws RecognitionException  {
		Expr result = null;


		Expr e1 =null;
		Expr e2 =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:116:2: (e1= rel_expr ( OR e2= rel_expr )* )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:116:4: e1= rel_expr ( OR e2= rel_expr )*
			{
			pushFollow(FOLLOW_rel_expr_in_or_expr520);
			e1=rel_expr();
			state._fsp--;

			result = e1;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:117:3: ( OR e2= rel_expr )*
			loop9:
			while (true) {
				int alt9=2;
				int LA9_0 = input.LA(1);
				if ( (LA9_0==OR) ) {
					alt9=1;
				}

				switch (alt9) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:117:4: OR e2= rel_expr
					{
					match(input,OR,FOLLOW_OR_in_or_expr528); 
					pushFollow(FOLLOW_rel_expr_in_or_expr532);
					e2=rel_expr();
					state._fsp--;


							result = new CompositeExpr(Ops.OR, result, e2);
							
					}
					break;

				default :
					break loop9;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "or_expr"



	// $ANTLR start "rel_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:124:1: rel_expr returns [Expr result] : e1= add_expr (o= rel_op e2= add_expr )? ;
	public final Expr rel_expr() throws RecognitionException  {
		Expr result = null;


		Expr e1 =null;
		Operator o =null;
		Expr e2 =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:125:2: (e1= add_expr (o= rel_op e2= add_expr )? )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:125:4: e1= add_expr (o= rel_op e2= add_expr )?
			{
			pushFollow(FOLLOW_add_expr_in_rel_expr558);
			e1=add_expr();
			state._fsp--;

			result = e1;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:126:3: (o= rel_op e2= add_expr )?
			int alt10=2;
			int LA10_0 = input.LA(1);
			if ( (LA10_0==EQ||(LA10_0 >= GEQ && LA10_0 <= GT)||(LA10_0 >= LEQ && LA10_0 <= LT)||LA10_0==NEQ) ) {
				alt10=1;
			}
			switch (alt10) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:126:4: o= rel_op e2= add_expr
					{
					pushFollow(FOLLOW_rel_op_in_rel_expr568);
					o=rel_op();
					state._fsp--;

					pushFollow(FOLLOW_add_expr_in_rel_expr572);
					e2=add_expr();
					state._fsp--;


							result = new CompositeExpr(o, result, e2);
							
					}
					break;

			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "rel_expr"



	// $ANTLR start "rel_op"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:133:1: rel_op returns [Operator op] : ( EQ | NEQ | GT | GEQ | LT | LEQ );
	public final Operator rel_op() throws RecognitionException  {
		Operator op = null;


		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:134:2: ( EQ | NEQ | GT | GEQ | LT | LEQ )
			int alt11=6;
			switch ( input.LA(1) ) {
			case EQ:
				{
				alt11=1;
				}
				break;
			case NEQ:
				{
				alt11=2;
				}
				break;
			case GT:
				{
				alt11=3;
				}
				break;
			case GEQ:
				{
				alt11=4;
				}
				break;
			case LT:
				{
				alt11=5;
				}
				break;
			case LEQ:
				{
				alt11=6;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}
			switch (alt11) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:134:4: EQ
					{
					match(input,EQ,FOLLOW_EQ_in_rel_op597); 
					op = Ops.EQU;
					}
					break;
				case 2 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:135:4: NEQ
					{
					match(input,NEQ,FOLLOW_NEQ_in_rel_op604); 
					op = Ops.NEQ;
					}
					break;
				case 3 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:136:4: GT
					{
					match(input,GT,FOLLOW_GT_in_rel_op611); 
					op = Ops.GT;
					}
					break;
				case 4 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:137:4: GEQ
					{
					match(input,GEQ,FOLLOW_GEQ_in_rel_op618); 
					op = Ops.GEQ;
					}
					break;
				case 5 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:138:4: LT
					{
					match(input,LT,FOLLOW_LT_in_rel_op625); 
					op = Ops.LT;
					}
					break;
				case 6 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:139:4: LEQ
					{
					match(input,LEQ,FOLLOW_LEQ_in_rel_op632); 
					op = Ops.LEQ;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return op;
	}
	// $ANTLR end "rel_op"



	// $ANTLR start "add_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:142:1: add_expr returns [Expr result] : e1= sub_expr ( '+' e2= sub_expr )* ;
	public final Expr add_expr() throws RecognitionException  {
		Expr result = null;


		Expr e1 =null;
		Expr e2 =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:143:2: (e1= sub_expr ( '+' e2= sub_expr )* )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:143:4: e1= sub_expr ( '+' e2= sub_expr )*
			{
			pushFollow(FOLLOW_sub_expr_in_add_expr652);
			e1=sub_expr();
			state._fsp--;

			result = e1;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:144:3: ( '+' e2= sub_expr )*
			loop12:
			while (true) {
				int alt12=2;
				int LA12_0 = input.LA(1);
				if ( (LA12_0==34) ) {
					alt12=1;
				}

				switch (alt12) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:144:4: '+' e2= sub_expr
					{
					match(input,34,FOLLOW_34_in_add_expr660); 
					pushFollow(FOLLOW_sub_expr_in_add_expr664);
					e2=sub_expr();
					state._fsp--;

					result = new CompositeExpr(Ops.PLUS, result, e2);
					}
					break;

				default :
					break loop12;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "add_expr"



	// $ANTLR start "sub_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:148:1: sub_expr returns [Expr result] : e1= mult_expr ( '-' e2= mult_expr )* ;
	public final Expr sub_expr() throws RecognitionException  {
		Expr result = null;


		Expr e1 =null;
		Expr e2 =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:149:2: (e1= mult_expr ( '-' e2= mult_expr )* )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:149:4: e1= mult_expr ( '-' e2= mult_expr )*
			{
			pushFollow(FOLLOW_mult_expr_in_sub_expr691);
			e1=mult_expr();
			state._fsp--;

			result = e1;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:150:3: ( '-' e2= mult_expr )*
			loop13:
			while (true) {
				int alt13=2;
				int LA13_0 = input.LA(1);
				if ( (LA13_0==36) ) {
					alt13=1;
				}

				switch (alt13) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:150:4: '-' e2= mult_expr
					{
					match(input,36,FOLLOW_36_in_sub_expr699); 
					pushFollow(FOLLOW_mult_expr_in_sub_expr703);
					e2=mult_expr();
					state._fsp--;

					result = new CompositeExpr(Ops.MINUS, result, e2);
					}
					break;

				default :
					break loop13;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "sub_expr"



	// $ANTLR start "mult_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:154:1: mult_expr returns [Expr result] : e1= qut_expr ( '*' e2= qut_expr )* ;
	public final Expr mult_expr() throws RecognitionException  {
		Expr result = null;


		Expr e1 =null;
		Expr e2 =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:155:2: (e1= qut_expr ( '*' e2= qut_expr )* )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:155:4: e1= qut_expr ( '*' e2= qut_expr )*
			{
			pushFollow(FOLLOW_qut_expr_in_mult_expr727);
			e1=qut_expr();
			state._fsp--;

			result = e1;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:156:3: ( '*' e2= qut_expr )*
			loop14:
			while (true) {
				int alt14=2;
				int LA14_0 = input.LA(1);
				if ( (LA14_0==33) ) {
					alt14=1;
				}

				switch (alt14) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:156:4: '*' e2= qut_expr
					{
					match(input,33,FOLLOW_33_in_mult_expr735); 
					pushFollow(FOLLOW_qut_expr_in_mult_expr740);
					e2=qut_expr();
					state._fsp--;

					result = new CompositeExpr(Ops.MULT, result, e2);
					}
					break;

				default :
					break loop14;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "mult_expr"



	// $ANTLR start "qut_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:158:1: qut_expr returns [Expr result] : e2= div_expr ( '/' e2= div_expr )* ;
	public final Expr qut_expr() throws RecognitionException  {
		Expr result = null;


		Expr e2 =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:159:2: (e2= div_expr ( '/' e2= div_expr )* )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:159:4: e2= div_expr ( '/' e2= div_expr )*
			{
			pushFollow(FOLLOW_div_expr_in_qut_expr760);
			e2=div_expr();
			state._fsp--;

			result = e2;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:160:3: ( '/' e2= div_expr )*
			loop15:
			while (true) {
				int alt15=2;
				int LA15_0 = input.LA(1);
				if ( (LA15_0==37) ) {
					alt15=1;
				}

				switch (alt15) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:160:4: '/' e2= div_expr
					{
					match(input,37,FOLLOW_37_in_qut_expr768); 
					pushFollow(FOLLOW_div_expr_in_qut_expr773);
					e2=div_expr();
					state._fsp--;

					result = new CompositeExpr(Ops.DIV, result, e2);
					}
					break;

				default :
					break loop15;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "qut_expr"



	// $ANTLR start "div_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:163:1: div_expr returns [Expr result] : e2= mod_expr ( DIV e2= mod_expr )* ;
	public final Expr div_expr() throws RecognitionException  {
		Expr result = null;


		Expr e2 =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:164:2: (e2= mod_expr ( DIV e2= mod_expr )* )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:164:4: e2= mod_expr ( DIV e2= mod_expr )*
			{
			pushFollow(FOLLOW_mod_expr_in_div_expr795);
			e2=mod_expr();
			state._fsp--;

			result = e2;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:165:3: ( DIV e2= mod_expr )*
			loop16:
			while (true) {
				int alt16=2;
				int LA16_0 = input.LA(1);
				if ( (LA16_0==DIV) ) {
					alt16=1;
				}

				switch (alt16) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:165:4: DIV e2= mod_expr
					{
					match(input,DIV,FOLLOW_DIV_in_div_expr803); 
					pushFollow(FOLLOW_mod_expr_in_div_expr808);
					e2=mod_expr();
					state._fsp--;

					result = new CompositeExpr(Ops.IDIV, result, e2);
					}
					break;

				default :
					break loop16;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "div_expr"



	// $ANTLR start "mod_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:167:1: mod_expr returns [Expr result] : e2= expt_expr ( MOD e2= expt_expr )* ;
	public final Expr mod_expr() throws RecognitionException  {
		Expr result = null;


		Expr e2 =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:168:2: (e2= expt_expr ( MOD e2= expt_expr )* )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:168:4: e2= expt_expr ( MOD e2= expt_expr )*
			{
			pushFollow(FOLLOW_expt_expr_in_mod_expr828);
			e2=expt_expr();
			state._fsp--;

			result = e2;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:169:3: ( MOD e2= expt_expr )*
			loop17:
			while (true) {
				int alt17=2;
				int LA17_0 = input.LA(1);
				if ( (LA17_0==MOD) ) {
					alt17=1;
				}

				switch (alt17) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:169:4: MOD e2= expt_expr
					{
					match(input,MOD,FOLLOW_MOD_in_mod_expr836); 
					pushFollow(FOLLOW_expt_expr_in_mod_expr841);
					e2=expt_expr();
					state._fsp--;

					result = new CompositeExpr(Ops.MOD, result, e2);
					}
					break;

				default :
					break loop17;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "mod_expr"



	// $ANTLR start "expt_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:172:1: expt_expr returns [Expr result] : e1= base_expr ( '^' e2= base_expr )* ;
	public final Expr expt_expr() throws RecognitionException  {
		Expr result = null;


		Expr e1 =null;
		Expr e2 =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:173:2: (e1= base_expr ( '^' e2= base_expr )* )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:173:4: e1= base_expr ( '^' e2= base_expr )*
			{
			pushFollow(FOLLOW_base_expr_in_expt_expr863);
			e1=base_expr();
			state._fsp--;

			result = e1;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:174:3: ( '^' e2= base_expr )*
			loop18:
			while (true) {
				int alt18=2;
				int LA18_0 = input.LA(1);
				if ( (LA18_0==40) ) {
					alt18=1;
				}

				switch (alt18) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:174:4: '^' e2= base_expr
					{
					match(input,40,FOLLOW_40_in_expt_expr871); 
					pushFollow(FOLLOW_base_expr_in_expt_expr875);
					e2=base_expr();
					state._fsp--;


							result = new CompositeExpr(Ops.EXPT, result, e2);
							
					}
					break;

				default :
					break loop18;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "expt_expr"



	// $ANTLR start "base_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:181:1: base_expr returns [Expr result] : ( INT | FLOAT | STRING | ID |f= func_call | '-' e1= base_expr | NOT e1= base_expr | '(' e1= expr ')' );
	public final Expr base_expr() throws RecognitionException  {
		Expr result = null;


		Token INT3=null;
		Token FLOAT4=null;
		Token STRING5=null;
		Token ID6=null;
		Expr f =null;
		Expr e1 =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:182:2: ( INT | FLOAT | STRING | ID |f= func_call | '-' e1= base_expr | NOT e1= base_expr | '(' e1= expr ')' )
			int alt19=8;
			switch ( input.LA(1) ) {
			case INT:
				{
				alt19=1;
				}
				break;
			case FLOAT:
				{
				alt19=2;
				}
				break;
			case STRING:
				{
				alt19=3;
				}
				break;
			case ID:
				{
				int LA19_4 = input.LA(2);
				if ( (LA19_4==31) ) {
					alt19=5;
				}
				else if ( (LA19_4==AND||(LA19_4 >= DIV && LA19_4 <= DO)||LA19_4==EQ||(LA19_4 >= GEQ && LA19_4 <= GT)||(LA19_4 >= LEQ && LA19_4 <= NEQ)||LA19_4==OR||LA19_4==THEN||(LA19_4 >= 32 && LA19_4 <= 38)||LA19_4==40) ) {
					alt19=4;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 19, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 36:
				{
				alt19=6;
				}
				break;
			case NOT:
				{
				alt19=7;
				}
				break;
			case 31:
				{
				alt19=8;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 19, 0, input);
				throw nvae;
			}
			switch (alt19) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:182:4: INT
					{
					INT3=(Token)match(input,INT,FOLLOW_INT_in_base_expr901); 
					result = new NumericExpr((INT3!=null?INT3.getText():null));
					}
					break;
				case 2 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:183:4: FLOAT
					{
					FLOAT4=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_base_expr909); 
					result = new NumericExpr((FLOAT4!=null?FLOAT4.getText():null));
					}
					break;
				case 3 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:184:5: STRING
					{
					STRING5=(Token)match(input,STRING,FOLLOW_STRING_in_base_expr918); 
					result = new StringExpr((STRING5!=null?STRING5.getText():null));
					}
					break;
				case 4 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:185:4: ID
					{
					ID6=(Token)match(input,ID,FOLLOW_ID_in_base_expr926); 
					result = new VarExpr((ID6!=null?ID6.getText():null));
					}
					break;
				case 5 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:186:4: f= func_call
					{
					pushFollow(FOLLOW_func_call_in_base_expr936);
					f=func_call();
					state._fsp--;

					result = f;
					}
					break;
				case 6 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:187:4: '-' e1= base_expr
					{
					match(input,36,FOLLOW_36_in_base_expr944); 
					pushFollow(FOLLOW_base_expr_in_base_expr948);
					e1=base_expr();
					state._fsp--;

					result = new CompositeExpr(Ops.UMINUS, e1);
					}
					break;
				case 7 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:188:4: NOT e1= base_expr
					{
					match(input,NOT,FOLLOW_NOT_in_base_expr955); 
					pushFollow(FOLLOW_base_expr_in_base_expr959);
					e1=base_expr();
					state._fsp--;

					result = new CompositeExpr(Ops.NOT, e1);
					}
					break;
				case 8 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:189:4: '(' e1= expr ')'
					{
					match(input,31,FOLLOW_31_in_base_expr966); 
					pushFollow(FOLLOW_expr_in_base_expr970);
					e1=expr();
					state._fsp--;

					match(input,32,FOLLOW_32_in_base_expr972); 
					result = e1;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "base_expr"



	// $ANTLR start "function_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:192:1: function_expr returns [Function result] : FUNCTION ID '(' (args= arg_list_text )? ')' b= block_sttm_function ;
	public final Function function_expr() throws RecognitionException  {
		Function result = null;


		Token ID7=null;
		List<String> args =null;
		BlockSttm b =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:193:2: ( FUNCTION ID '(' (args= arg_list_text )? ')' b= block_sttm_function )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:194:3: FUNCTION ID '(' (args= arg_list_text )? ')' b= block_sttm_function
			{
			match(input,FUNCTION,FOLLOW_FUNCTION_in_function_expr993); 
			ID7=(Token)match(input,ID,FOLLOW_ID_in_function_expr995); 
			match(input,31,FOLLOW_31_in_function_expr997); 
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:194:23: (args= arg_list_text )?
			int alt20=2;
			int LA20_0 = input.LA(1);
			if ( (LA20_0==ID) ) {
				alt20=1;
			}
			switch (alt20) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:194:23: args= arg_list_text
					{
					pushFollow(FOLLOW_arg_list_text_in_function_expr1001);
					args=arg_list_text();
					state._fsp--;

					}
					break;

			}

			match(input,32,FOLLOW_32_in_function_expr1004); 
			pushFollow(FOLLOW_block_sttm_function_in_function_expr1008);
			b=block_sttm_function();
			state._fsp--;


						result = new Function((ID7!=null?ID7.getText():null), args, b);
					
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "function_expr"



	// $ANTLR start "function_call"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:200:1: function_call returns [FunctionCall result] : FUNCTION ID '(' (args= arg_list )? ')' ';' ;
	public final FunctionCall function_call() throws RecognitionException  {
		FunctionCall result = null;


		Token ID8=null;
		List<Expr> args =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:201:2: ( FUNCTION ID '(' (args= arg_list )? ')' ';' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:202:3: FUNCTION ID '(' (args= arg_list )? ')' ';'
			{
			match(input,FUNCTION,FOLLOW_FUNCTION_in_function_call1030); 
			ID8=(Token)match(input,ID,FOLLOW_ID_in_function_call1032); 
			match(input,31,FOLLOW_31_in_function_call1034); 
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:202:23: (args= arg_list )?
			int alt21=2;
			int LA21_0 = input.LA(1);
			if ( (LA21_0==FLOAT||LA21_0==ID||LA21_0==INT||LA21_0==NOT||LA21_0==STRING||LA21_0==31||LA21_0==36) ) {
				alt21=1;
			}
			switch (alt21) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:202:23: args= arg_list
					{
					pushFollow(FOLLOW_arg_list_in_function_call1038);
					args=arg_list();
					state._fsp--;

					}
					break;

			}

			match(input,32,FOLLOW_32_in_function_call1041); 
			match(input,38,FOLLOW_38_in_function_call1043); 
			 
						result = new FunctionCall((ID8!=null?ID8.getText():null), args);
					
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "function_call"



	// $ANTLR start "func_call"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:208:1: func_call returns [Expr result] : ID '(' (args= arg_list )? ')' ;
	public final Expr func_call() throws RecognitionException  {
		Expr result = null;


		Token ID9=null;
		List<Expr> args =null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:209:2: ( ID '(' (args= arg_list )? ')' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:209:4: ID '(' (args= arg_list )? ')'
			{
			ID9=(Token)match(input,ID,FOLLOW_ID_in_func_call1063); 
			match(input,31,FOLLOW_31_in_func_call1065); 
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:209:15: (args= arg_list )?
			int alt22=2;
			int LA22_0 = input.LA(1);
			if ( (LA22_0==FLOAT||LA22_0==ID||LA22_0==INT||LA22_0==NOT||LA22_0==STRING||LA22_0==31||LA22_0==36) ) {
				alt22=1;
			}
			switch (alt22) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:209:15: args= arg_list
					{
					pushFollow(FOLLOW_arg_list_in_func_call1069);
					args=arg_list();
					state._fsp--;

					}
					break;

			}

			match(input,32,FOLLOW_32_in_func_call1072); 
					
					Operator fn = Func.find((ID9!=null?ID9.getText():null));
					List<Expr> argList = args;
					if (fn != null){
						result = new CompositeExpr(fn, argList);
					}else{
						result = new FunctionCall((ID9!=null?ID9.getText():null), argList);
					}
					
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "func_call"



	// $ANTLR start "string_expr"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:221:1: string_expr returns [String result] : STRING ;
	public final String string_expr() throws RecognitionException  {
		String result = null;


		Token STRING10=null;

		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:222:2: ( STRING )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:223:3: STRING
			{
			STRING10=(Token)match(input,STRING,FOLLOW_STRING_in_string_expr1094); 
			result = (STRING10!=null?STRING10.getText():null);
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "string_expr"



	// $ANTLR start "arg_list"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:226:1: arg_list returns [List<Expr> list] : e1= expr ( ',' e2= expr )* ;
	public final List<Expr> arg_list() throws RecognitionException  {
		List<Expr> list = null;


		Expr e1 =null;
		Expr e2 =null;


			list = new LinkedList<Expr>();
			
		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:230:2: (e1= expr ( ',' e2= expr )* )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:230:4: e1= expr ( ',' e2= expr )*
			{
			pushFollow(FOLLOW_expr_in_arg_list1120);
			e1=expr();
			state._fsp--;

			list.add(e1);
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:231:3: ( ',' e2= expr )*
			loop23:
			while (true) {
				int alt23=2;
				int LA23_0 = input.LA(1);
				if ( (LA23_0==35) ) {
					alt23=1;
				}

				switch (alt23) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:231:4: ',' e2= expr
					{
					match(input,35,FOLLOW_35_in_arg_list1128); 
					pushFollow(FOLLOW_expr_in_arg_list1132);
					e2=expr();
					state._fsp--;

					list.add(e2);
					}
					break;

				default :
					break loop23;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return list;
	}
	// $ANTLR end "arg_list"



	// $ANTLR start "arg_list_text"
	// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:235:1: arg_list_text returns [List<String> list] : e1= ID ( ',' e2= ID )* ;
	public final List<String> arg_list_text() throws RecognitionException  {
		List<String> list = null;


		Token e1=null;
		Token e2=null;


			list = new LinkedList<String>();
			
		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:239:2: (e1= ID ( ',' e2= ID )* )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:239:4: e1= ID ( ',' e2= ID )*
			{
			e1=(Token)match(input,ID,FOLLOW_ID_in_arg_list_text1164); 
			list.add((e1!=null?e1.getText():null));
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:240:3: ( ',' e2= ID )*
			loop24:
			while (true) {
				int alt24=2;
				int LA24_0 = input.LA(1);
				if ( (LA24_0==35) ) {
					alt24=1;
				}

				switch (alt24) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:240:4: ',' e2= ID
					{
					match(input,35,FOLLOW_35_in_arg_list_text1172); 
					e2=(Token)match(input,ID,FOLLOW_ID_in_arg_list_text1177); 
					list.add((e2!=null?e2.getText():null));
					}
					break;

				default :
					break loop24;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return list;
	}
	// $ANTLR end "arg_list_text"

	// Delegated rules



	public static final BitSet FOLLOW_sttm_in_prog38 = new BitSet(new long[]{0x00000010A90E7082L});
	public static final BitSet FOLLOW_assign_in_sttm60 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_sttm62 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr_in_sttm71 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_sttm73 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_if_sttm_in_sttm83 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_while_sttm_in_sttm92 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_for_sttm_in_sttm102 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_block_sttm_in_sttm111 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_function_expr_in_sttm121 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_assign139 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_assign141 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_expr_in_assign145 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_assign_temp170 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_assign_temp172 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_expr_in_assign_temp176 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_if_sttm201 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_expr_in_if_sttm205 = new BitSet(new long[]{0x0000000010000000L});
	public static final BitSet FOLLOW_THEN_in_if_sttm207 = new BitSet(new long[]{0x00000010A90E7080L});
	public static final BitSet FOLLOW_sttm_in_if_sttm211 = new BitSet(new long[]{0x0000000000000300L});
	public static final BitSet FOLLOW_ELSE_in_if_sttm214 = new BitSet(new long[]{0x00000010A90E7080L});
	public static final BitSet FOLLOW_sttm_in_if_sttm218 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_END_in_if_sttm230 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHILE_in_while_sttm247 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_expr_in_while_sttm251 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_block_sttm_in_while_sttm255 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FOR_in_for_sttm275 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_assign_temp_in_for_sttm279 = new BitSet(new long[]{0x0000000800000000L});
	public static final BitSet FOLLOW_35_in_for_sttm281 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_base_expr_in_for_sttm285 = new BitSet(new long[]{0x0000000800000000L});
	public static final BitSet FOLLOW_35_in_for_sttm287 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_base_expr_in_for_sttm291 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_block_sttm_for_in_for_sttm295 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DO_in_block_sttm325 = new BitSet(new long[]{0x00000010A90E7280L});
	public static final BitSet FOLLOW_sttm_in_block_sttm332 = new BitSet(new long[]{0x00000010A90E7280L});
	public static final BitSet FOLLOW_END_in_block_sttm340 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sttm_in_block_sttm_function372 = new BitSet(new long[]{0x00000010AD0E7280L});
	public static final BitSet FOLLOW_RETURN_in_block_sttm_function381 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_expr_in_block_sttm_function385 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_block_sttm_function387 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_END_in_block_sttm_function395 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DO_in_block_sttm_for421 = new BitSet(new long[]{0x00000010A90E7280L});
	public static final BitSet FOLLOW_sttm_in_block_sttm_for428 = new BitSet(new long[]{0x00000010A90E7280L});
	public static final BitSet FOLLOW_END_in_block_sttm_for436 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_and_expr_in_expr458 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_or_expr_in_and_expr480 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_AND_in_and_expr488 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_or_expr_in_and_expr492 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_rel_expr_in_or_expr520 = new BitSet(new long[]{0x0000000002000002L});
	public static final BitSet FOLLOW_OR_in_or_expr528 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_rel_expr_in_or_expr532 = new BitSet(new long[]{0x0000000002000002L});
	public static final BitSet FOLLOW_add_expr_in_rel_expr558 = new BitSet(new long[]{0x0000000000B18402L});
	public static final BitSet FOLLOW_rel_op_in_rel_expr568 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_add_expr_in_rel_expr572 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_EQ_in_rel_op597 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEQ_in_rel_op604 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GT_in_rel_op611 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GEQ_in_rel_op618 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LT_in_rel_op625 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LEQ_in_rel_op632 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sub_expr_in_add_expr652 = new BitSet(new long[]{0x0000000400000002L});
	public static final BitSet FOLLOW_34_in_add_expr660 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_sub_expr_in_add_expr664 = new BitSet(new long[]{0x0000000400000002L});
	public static final BitSet FOLLOW_mult_expr_in_sub_expr691 = new BitSet(new long[]{0x0000001000000002L});
	public static final BitSet FOLLOW_36_in_sub_expr699 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_mult_expr_in_sub_expr703 = new BitSet(new long[]{0x0000001000000002L});
	public static final BitSet FOLLOW_qut_expr_in_mult_expr727 = new BitSet(new long[]{0x0000000200000002L});
	public static final BitSet FOLLOW_33_in_mult_expr735 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_qut_expr_in_mult_expr740 = new BitSet(new long[]{0x0000000200000002L});
	public static final BitSet FOLLOW_div_expr_in_qut_expr760 = new BitSet(new long[]{0x0000002000000002L});
	public static final BitSet FOLLOW_37_in_qut_expr768 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_div_expr_in_qut_expr773 = new BitSet(new long[]{0x0000002000000002L});
	public static final BitSet FOLLOW_mod_expr_in_div_expr795 = new BitSet(new long[]{0x0000000000000042L});
	public static final BitSet FOLLOW_DIV_in_div_expr803 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_mod_expr_in_div_expr808 = new BitSet(new long[]{0x0000000000000042L});
	public static final BitSet FOLLOW_expt_expr_in_mod_expr828 = new BitSet(new long[]{0x0000000000400002L});
	public static final BitSet FOLLOW_MOD_in_mod_expr836 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_expt_expr_in_mod_expr841 = new BitSet(new long[]{0x0000000000400002L});
	public static final BitSet FOLLOW_base_expr_in_expt_expr863 = new BitSet(new long[]{0x0000010000000002L});
	public static final BitSet FOLLOW_40_in_expt_expr871 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_base_expr_in_expt_expr875 = new BitSet(new long[]{0x0000010000000002L});
	public static final BitSet FOLLOW_INT_in_base_expr901 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FLOAT_in_base_expr909 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STRING_in_base_expr918 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_base_expr926 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_func_call_in_base_expr936 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_36_in_base_expr944 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_base_expr_in_base_expr948 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_in_base_expr955 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_base_expr_in_base_expr959 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_31_in_base_expr966 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_expr_in_base_expr970 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_32_in_base_expr972 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FUNCTION_in_function_expr993 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_ID_in_function_expr995 = new BitSet(new long[]{0x0000000080000000L});
	public static final BitSet FOLLOW_31_in_function_expr997 = new BitSet(new long[]{0x0000000100020000L});
	public static final BitSet FOLLOW_arg_list_text_in_function_expr1001 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_32_in_function_expr1004 = new BitSet(new long[]{0x00000010AD0E7280L});
	public static final BitSet FOLLOW_block_sttm_function_in_function_expr1008 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FUNCTION_in_function_call1030 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_ID_in_function_call1032 = new BitSet(new long[]{0x0000000080000000L});
	public static final BitSet FOLLOW_31_in_function_call1034 = new BitSet(new long[]{0x00000011890A1000L});
	public static final BitSet FOLLOW_arg_list_in_function_call1038 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_32_in_function_call1041 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_function_call1043 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_func_call1063 = new BitSet(new long[]{0x0000000080000000L});
	public static final BitSet FOLLOW_31_in_func_call1065 = new BitSet(new long[]{0x00000011890A1000L});
	public static final BitSet FOLLOW_arg_list_in_func_call1069 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_32_in_func_call1072 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STRING_in_string_expr1094 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr_in_arg_list1120 = new BitSet(new long[]{0x0000000800000002L});
	public static final BitSet FOLLOW_35_in_arg_list1128 = new BitSet(new long[]{0x00000010890A1000L});
	public static final BitSet FOLLOW_expr_in_arg_list1132 = new BitSet(new long[]{0x0000000800000002L});
	public static final BitSet FOLLOW_ID_in_arg_list_text1164 = new BitSet(new long[]{0x0000000800000002L});
	public static final BitSet FOLLOW_35_in_arg_list_text1172 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_ID_in_arg_list_text1177 = new BitSet(new long[]{0x0000000800000002L});
}
