/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

/**
 *
 * @author Vagner
 */
public class ForSttm extends Statement{
    
    private final Statement doSttm;
    private final Statement inicio;
    private final Expr limite;
    private final Expr interator;

    public ForSttm(Statement inicio, Expr limite, Expr interator, Statement doSttm) {
        this.doSttm = doSttm;
        this.inicio = inicio;
        this.limite = limite;
        this.interator = interator;
    }
    
    @Override
    public Object exec(Context ctx) {
        Object result = null;
        double begin = (double)inicio.exec(ctx);
        if(begin > (Double)limite.eval(ctx)){
            for (double i = begin; i >= (Double)limite.eval(ctx); i = i + (Double)interator.eval(ctx)) {
                ctx.put("i", i);
                result = doSttm.exec(ctx);                
            }
        }else{
            for (double i = begin; i <= (Double)limite.eval(ctx); i = i + (Double)interator.eval(ctx)) {
                ctx.put("i", i);
                result = doSttm.exec(ctx);                
            }
        }        
        return result;
    }
    
}
