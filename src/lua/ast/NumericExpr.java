/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

/**
 *
 * @author 1208144
 */
public class NumericExpr extends Expr implements Valor{
    
    private final Double value;

    public NumericExpr(Double value) {
        this.value = value;
    }

    public NumericExpr(String t) {
        this(Double.parseDouble(t));
    }
    
    public NumericExpr(Object t) {
        this((Double)t);
    }
    
    @Override
    public Double eval(Context ctx) {
        return value;
    }
    
    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public Double menorQue(Object elemento) {
        
        return this.value < (Double)elemento ? 1.0 : 0.0;
    }
    
    @Override
    public Double menorIgualQue(Object elemento) {
        
        return this.value <= (Double)elemento ? 1.0 : 0.0;
    }
    
    @Override
    public Double maiorQue(Object elemento) {
        
        return this.value > (Double)elemento ? 1.0 : 0.0;
    }
    
    @Override
    public Double maiorIgualQue(Object elemento) {
        
        return this.value >= (Double)elemento ? 1.0 : 0.0;
    }
}
