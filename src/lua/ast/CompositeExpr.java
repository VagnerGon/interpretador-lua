/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author 1208144
 */
public class CompositeExpr extends Expr {
    
    protected Operator op;
    protected List<Expr> args;
    
    public CompositeExpr(Operator op, List<Expr> args) {
        this.op = op;
        this.args = args;
    }
    
    public CompositeExpr(Operator op, Expr... args) {
        this(op, Arrays.asList(args));
    }
    
    @Override
    public Double eval(Context ctx) {
        Double value = op.eval(ctx, args);
        return value;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        sb.append(op.toString());
        for (Expr e: args) {
            String t = e.toString();
            sb.append("\u0020");
            sb.append(t);
        }
        sb.append(")");
        return sb.toString();
    }
}
