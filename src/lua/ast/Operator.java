/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

import java.util.List;

/**
 *
 * @author 1208144
 */
public abstract class Operator {
    public abstract Double eval(Context ctx, List<Expr> args);
}
