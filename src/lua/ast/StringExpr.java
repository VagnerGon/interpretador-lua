/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

/**
 *
 * @author Vagner
 */
public class StringExpr extends Expr implements Valor{

    private final String string;

    public StringExpr(String string) {        
                
        if(string.charAt(0) == '"' && string.charAt(string.length()-1) == '"')
            this.string = string.substring(1, string.length()-1);
        else            
            this.string = string;
    }        

    @Override
    public Object eval(Context ctx) {
        return this.string;
    }

    @Override
    public Double menorQue(Object elemento) {        
        if(this.string.compareTo(elemento.toString()) < 0)
            return 1.0;
        return 0.0;
    }

    @Override
    public Double menorIgualQue(Object elemento) {
        if(this.string.compareTo(elemento.toString()) <= 0)
            return 1.0;
        return 0.0;
    }

    @Override
    public Double maiorQue(Object elemento) {
        if(this.string.compareTo(elemento.toString()) > 0)
            return 1.0;
        return 0.0;
    }

    @Override
    public Double maiorIgualQue(Object elemento) {
        if(this.string.compareTo(elemento.toString()) >= 0)
            return 1.0;
        return 0.0;
    }
}
