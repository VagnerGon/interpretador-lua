/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

/**
 *
 * @author 1208144
 */
public class Assign extends Statement {

    private final String varName;
    private final Expr rhsExpr;

    public Assign(String varName, Expr rhs) {
        assert (varName != null);
        assert (rhs != null);
        this.varName = varName;
        this.rhsExpr = rhs;
    }
    
    public String getKey(){
        return this.varName;
    }

    @Override
    public Object exec(Context ctx) {
        Object result = rhsExpr.eval(ctx);
        ctx.put(varName, result);
        return result;
    }

    @Override
    public String toString() {
        String fmt = "%s = %s";
        return String.format(fmt, varName, rhsExpr.toString());
    }
}
