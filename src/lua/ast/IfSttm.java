/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

/**
 *
 * @author 1208144
 */
public class IfSttm extends Statement {
    private final Expr testExpr;
    private final Statement thenSttm;
    private final Statement elseSttm;
    
    public IfSttm(Expr testExpr, Statement thenSttm, Statement elseSttm) {
        assert(testExpr != null);
        assert(thenSttm != null);
        this.testExpr = testExpr;
        this.thenSttm = thenSttm;
        this.elseSttm = elseSttm;
    }
    
    public IfSttm(Expr testExpr, Statement thenSttm) {
        this(testExpr, thenSttm, null);
    }

    @Override
    public Object exec(Context ctx) {
        Object result = null;
        Double test = (Double)testExpr.eval(ctx);
        if (test != 0) {
            result = thenSttm.exec(ctx);
        }
        else if (elseSttm != null) {
            result = elseSttm.exec(ctx);
        }
        return result;
    }
 
    @Override
    public String toString() {
        if (elseSttm == null) {
            String fmt = "if %s \nthen %s";
            return String.format(fmt, testExpr.toString(), thenSttm.toString());
        }
        else {
            String fmt = "if %s \nthen %s \nelse %s";
            return String.format(fmt,
                    testExpr.toString(), 
                    thenSttm.toString(),
                    elseSttm.toString());
        }
    }
}
