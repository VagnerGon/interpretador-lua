/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

import java.util.List;

/**
 *
 * @author Vagner
 */
public class Function extends Statement{
    
    private final String id;
    private final List<String> args;
    private final List<Statement> sttmList;

    public Function(String id, List<String> args, BlockSttm sttmList) {
        this.id = id;
        this.args = args;
        this.sttmList = sttmList.getStatements();
    }
    
    @Override
    public Object exec(Context ctx) {
        ctx.put(id, this);
        return sttmList;
    }
    
    public List<String> getArgs(){
        return this.args;
    }
    
    public List<Statement> getSttmList(){
        return this.sttmList;
    }
    
    public String getNome(){
        return this.id;
    }
}
