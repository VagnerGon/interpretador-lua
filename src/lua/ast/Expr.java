/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

/**
 *
 * @author 1208144
 */
public abstract class Expr extends Statement {
    
    public abstract Object eval(Context ctx);
    
    @Override
    public Object exec(Context ctx) {
        Object result = eval(ctx);
        return result;
    }   
    
}
