/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

import java.util.List;

/**
 *
 * @author 1208144
 */
public class Ops {

    public static final Operator PLUS = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            double soma = 0;
            for (Expr e: args) {
                soma += (Double)e.eval(ctx);
            }
            return soma;
        }
        
        @Override
        public String toString() {
            return "+";
        }
    };
    
    public static final Operator MINUS = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() > 0);
            double acc = (Double)args.get(0).eval(ctx);
            for (int i = 1; i < args.size(); i++) {
                acc -= (Double)args.get(i).eval(ctx);
            }
            return acc;
        }
        
        @Override
        public String toString() {
            return "-";
        }
    };
    
    public static final Operator UMINUS = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() == 1);
            double acc = (Double)args.get(0).eval(ctx);
            return -acc;
        }
        
        @Override
        public String toString() {
            return "-";
        }
    };
    
    public static final Operator MULT = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            double acc = 1;
            for (Expr e: args) {
                acc *= (Double)e.eval(ctx);
            }
            return acc;
        }
        
        @Override
        public String toString() {
            return "*";
        }
    };
    
    public static final Operator DIV = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() > 0);
            double acc = (Double)args.get(0).eval(ctx);
            for (int i = 1; i < args.size(); i++) {
                acc /= (Double)args.get(i).eval(ctx);
            }
            return acc;
        }
        
        @Override
        public String toString() {
            return "/";
        }
    };
    
    public static final Operator IDIV = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() > 0);           
            double acc = (Double)args.get(0).eval(ctx);
            for (int i = 1; i < args.size(); i++) {
                double quot = (Double)args.get(i).eval(ctx);
                acc = (long)(acc / quot);
            }
            return acc;
        }
        
        @Override
        public String toString() {
            return "div";
        }
    };
    
    public static final Operator MOD = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() > 0);           
            double acc = (Double)args.get(0).eval(ctx);
            for (int i = 1; i < args.size(); i++) {
                double d = (Double)args.get(i).eval(ctx);
                acc = rem(acc,d);
            }
            return acc;
        }
        
        @Override
        public String toString() {
            return "mod";
        }
    };
    
    private static double quot(double x, double y) {
        double q = (long)(x / y);
        return q;
    }
    
    private static double rem(double x, double y) {
        double q = quot(x,y);
        double r = x - (y * q);
        return r;
    }
    
    public static final Operator EXPT = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() > 0);           
            double acc = (Double)args.get(0).eval(ctx);
            for (int i = 1; i < args.size(); i++) {
                double x = (Double)args.get(i).eval(ctx);
                acc = Math.pow(acc,x);
            }
            return acc;
        }
        
        @Override
        public String toString() {
            return "^";
        }
    };
    
    public static final Operator EQU = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() == 2);
            Object lhs = args.get(0).eval(ctx);
            Object rhs = args.get(1).eval(ctx);
            return (lhs.equals(rhs)) ? 1.0 : 0.0;
        }
        
        @Override
        public String toString() {
            return "==";
        }
    };
    
    public static final Operator NEQ = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() == 2);
            Object lhs = args.get(0).eval(ctx);
            Object rhs = args.get(1).eval(ctx);
            return (!lhs.equals(rhs)) ? 1.0 : 0.0;
        }
        
        @Override
        public String toString() {
            return "!=";
        }
    };
    
    public static final Operator GT = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() == 2);
            Object lhs = args.get(0).eval(ctx);
            Object rhs = args.get(1).eval(ctx);
            if(lhs instanceof Double)                
                return new NumericExpr((Double)lhs).maiorQue(rhs);            
            else              
                return new StringExpr((String)lhs).maiorQue(rhs); 
        }
        
        @Override
        public String toString() {
            return ">";
        }
    };
    
    public static final Operator GEQ = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() == 2);
            Object lhs = args.get(0).eval(ctx);
            Object rhs = args.get(1).eval(ctx);
            if(lhs instanceof Double)                
                return new NumericExpr((Double)lhs).maiorIgualQue(rhs);            
            else              
                return new StringExpr((String)lhs).maiorIgualQue(rhs); 
        }
        
        @Override
        public String toString() {
            return ">=";
        }
    };
    
    public static final Operator LT = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() == 2);
            Object lhs = args.get(0).eval(ctx);
            Object rhs = args.get(1).eval(ctx);
            if(lhs instanceof Double)                
                return new NumericExpr(lhs).menorQue(rhs);            
            else              
                return new StringExpr((String)lhs).menorQue(rhs);  
        }
        
        @Override
        public String toString() {
            return "<";
        }
    };
    
    public static final Operator LEQ = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() == 2);
            Object lhs = args.get(0).eval(ctx);
            Object rhs = args.get(1).eval(ctx);           
            if(lhs instanceof Double)                
                return new NumericExpr(lhs).menorIgualQue(rhs);            
            else              
                return new StringExpr((String)lhs).menorIgualQue(rhs);  
        }
        
        @Override
        public String toString() {
            return "<=";
        }
    };
    
    public static final Operator AND = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            int acc = 1;
            for (Expr e: args) {
                double r = (Double)e.eval(ctx);
                acc = acc * (int)Math.signum(r);
            }
            return (double)acc;
        }
        
        @Override
        public String toString() {
            return "and";
        }
    };
                
    public static final Operator OR = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            double acc = 0;
            for (Expr e: args) {
                double r = (Double)e.eval(ctx);
                acc += r;
            }
            return (acc > 0) ? 1.0 : 0;
        }
        
        @Override
        public String toString() {
            return "or";
        }
    };
        
    public static final Operator NOT = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() == 1);
            double r = (Double)args.get(0).eval(ctx);
            return (r == 0) ? 1.0 : 0;
        }
        
        @Override
        public String toString() {
            return "not";
        }
    };
        
}
