/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

/**
 *
 * @author 1208144
 */
public class WhileSttm extends Statement {
    private final Expr testExpr;
    private final Statement doSttm;
    
    public WhileSttm(Expr testExpr, Statement doSttm) {
        assert(testExpr != null);
        assert(doSttm != null);
        this.testExpr = testExpr;
        this.doSttm = doSttm;
    }

    @Override
    public Object exec(Context ctx) {
        Object result = null;
        Object test = testExpr.eval(ctx);
        while (test != 0) {
            result = doSttm.exec(ctx);
            test = testExpr.eval(ctx);
        }
        return result;
    }
            
    @Override
    public String toString() {
        String fmt = "while %s do %s";
        return fmt.format(testExpr.toString(), doSttm.toString());
    }
}
