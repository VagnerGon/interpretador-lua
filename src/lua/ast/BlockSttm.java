/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

import java.util.List;

/**
 *
 * @author 1208144
 */
public class BlockSttm extends Statement{
    
    protected final List<Statement> sttmList;

    public BlockSttm(List<Statement> sttmList) {
        assert(sttmList != null);
        this.sttmList = sttmList;
    }

    @Override
    public Object exec(Context ctx) {
        Object result = null;        
        for (Statement s: sttmList) {
            result = s.exec(ctx);
        }
        return result;
    }
    
    public List<Statement> getStatements(){
        return this.sttmList;        
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{").append("\n");
        for (Statement s: sttmList) {
            String ln = s.toString();
            sb.append(ln).append(";").append("\n");
        }
        return sb.toString();
    }
    
}
