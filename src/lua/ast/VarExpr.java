/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

/**
 *
 * @author 1208144
 */
public class VarExpr extends Expr {
    
    private final String varName;

    public VarExpr(String varName) {
        this.varName = varName;
    }
    
    @Override
    public Object eval(Context ctx) {
        Object value = ctx.get(varName);
        return value;
    }
    
    @Override
    public String toString() {
        return varName;
    }
    
}
