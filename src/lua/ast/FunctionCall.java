/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

import java.util.List;

/**
 *
 * @author Vagner
 */
public class FunctionCall extends Expr {
    
    private final String id;
    private final List<Expr> args;
    
    public FunctionCall(String id, List<Expr> args) {
        this.id = id;
        this.args = args;
    }

    @Override
    public Object eval(Context ctx) {
        //Recebe a funcao chamada
        Function f = ctx.getFunc(id);
        //Verifica se a funcao existe
        assert(f != null);
        //Cria um novo escopo para a funcao
        Context novo = new Context();
        int i = 0;        
       
        //Adiciona todas as funcoes do contexto externo no contexto da funcao
        //[Permite recursao e chamadas externas]
        for(Function func : ctx.getFunctions()){
            novo.put(func.getNome(), func);
        }
        
        //Executa todos os Stataments da funcao
        for(Expr e : this.args){            
            novo.put(f.getArgs().get(i), e.eval(ctx));
            i++;
        }
        
        Object result = null;        
        for (Statement s: f.getSttmList()) {
            result = s.exec(novo);
        }
        return result;
    }
}
