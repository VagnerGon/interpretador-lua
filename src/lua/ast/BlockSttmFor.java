/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

import java.util.List;

/**
 *
 * @author 1208144
 */

//Bloco especifico para FOR
public class BlockSttmFor extends BlockSttm{

    public BlockSttmFor(List<Statement> sttmList) {
        super(sttmList);
    }  
    
    //Executa o Statemant normalmente, mas no final limpa a variavel de proprio contexto
    @Override
    public Object exec(Context ctx) {        
        Object o = super.exec(ctx);
        ctx.limparTemp();
        return o;
    }
}
