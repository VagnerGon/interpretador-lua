/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

import java.util.List;

/**
 *
 * @author 1208144
 */
public class Func {
    
    public static Operator find(String fn) {
        switch (fn) {
            case "sin": return SIN;
            case "cos": return COS;
            case "tan": return TAN;
            case "print": return PRINT;
            default:
                return null;
        }
    }

    public static final Operator SIN = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() == 1);
            double x = (Double)args.get(0).eval(ctx);
            return Math.sin(x);
        }
        
        @Override
        public String toString() {
            return "sin";
        }
    };
    
    public static final Operator COS = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() == 1);
            double x = (Double)args.get(0).eval(ctx);
            return Math.cos(x);
        }
        
        @Override
        public String toString() {
            return "cos";
        }
    };
        
    public static final Operator TAN = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() == 1);
            double x = (Double)args.get(0).eval(ctx);
            return Math.tan(x);
        }
        
        @Override
        public String toString() {
            return "tan";
        }
    };
    
    public static final Operator PRINT = new Operator() {
        @Override
        public Double eval(Context ctx, List<Expr> args) {
            assert(args.size() == 1);
            Object x = args.get(0).eval(ctx);
            System.out.println(x);
            return null;
        }
        
        @Override
        public String toString() {
            return "print";
        }
    };
    
}
