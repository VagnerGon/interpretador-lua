/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

/**
 *
 * @author Vagner
 */
interface Valor {
    
    public Double menorQue(Object elemento);
    
    public Double menorIgualQue(Object elemento);
    
    public Double maiorQue(Object elemento);
    
    public Double maiorIgualQue(Object elemento);
}
