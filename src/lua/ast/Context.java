/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author 1208144
 */
public class Context {
    
    protected HashMap<String,Object> varAssoc = new HashMap<String,Object>();
    protected HashMap<String,Function> funcAssoc = new HashMap<String,Function>();
    private List<String> temporarias = new LinkedList<>();

    public void put(String key, Double value) {
        varAssoc.put(key, value);
    }
    
    public void put(String key, Function value) {
        funcAssoc.put(key, value);
    }
    
    public void put(String key, String value) {
        varAssoc.put(key, value);
    }
    
    public void put(String key, Object value) {
        varAssoc.put(key, value);
    }
        
    public Object get(String key) {
        return varAssoc.get(key);
    }
    
    public Function getFunc(String key){
        return this.funcAssoc.get(key);
    }
    
    public List<Function> getFunctions(){
        return new LinkedList<Function>(this.funcAssoc.values());
    }
    
    public void putTemp(String key){
        this.temporarias.add(key);
    }
    
    public void limparTemp(){
        for(String key : this.temporarias)
            this.varAssoc.remove(key);
    }
}
