/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lua.ast;

/**
 *
 * @author 1208144
 */

//Assign de FOR
public class AssignTemp extends Assign {

    public AssignTemp(String varName, Expr rhs) {
        super(varName, rhs);
    }
    
    //Coloca a variavel no contexto geral
    @Override
    public Object exec(Context ctx) {
        Object o = super.exec(ctx);
        //Adiciona a variavel do bloco proprio numa lista de temporaria
        ctx.putTemp(super.getKey());
        return o;
    }

}
