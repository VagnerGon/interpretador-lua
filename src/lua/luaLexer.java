// $ANTLR 3.5 C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g 2013-04-19 17:03:19

package lua;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class luaLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__31=31;
	public static final int T__32=32;
	public static final int T__33=33;
	public static final int T__34=34;
	public static final int T__35=35;
	public static final int T__36=36;
	public static final int T__37=37;
	public static final int T__38=38;
	public static final int T__39=39;
	public static final int T__40=40;
	public static final int AND=4;
	public static final int COMMENT=5;
	public static final int DIV=6;
	public static final int DO=7;
	public static final int ELSE=8;
	public static final int END=9;
	public static final int EQ=10;
	public static final int EXPONENT=11;
	public static final int FLOAT=12;
	public static final int FOR=13;
	public static final int FUNCTION=14;
	public static final int GEQ=15;
	public static final int GT=16;
	public static final int ID=17;
	public static final int IF=18;
	public static final int INT=19;
	public static final int LEQ=20;
	public static final int LT=21;
	public static final int MOD=22;
	public static final int NEQ=23;
	public static final int NOT=24;
	public static final int OR=25;
	public static final int RETURN=26;
	public static final int STRING=27;
	public static final int THEN=28;
	public static final int WHILE=29;
	public static final int WS=30;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public luaLexer() {} 
	public luaLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public luaLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g"; }

	// $ANTLR start "T__31"
	public final void mT__31() throws RecognitionException {
		try {
			int _type = T__31;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:6:7: ( '(' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:6:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__31"

	// $ANTLR start "T__32"
	public final void mT__32() throws RecognitionException {
		try {
			int _type = T__32;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:7:7: ( ')' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:7:9: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__32"

	// $ANTLR start "T__33"
	public final void mT__33() throws RecognitionException {
		try {
			int _type = T__33;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:8:7: ( '*' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:8:9: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__33"

	// $ANTLR start "T__34"
	public final void mT__34() throws RecognitionException {
		try {
			int _type = T__34;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:9:7: ( '+' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:9:9: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__34"

	// $ANTLR start "T__35"
	public final void mT__35() throws RecognitionException {
		try {
			int _type = T__35;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:10:7: ( ',' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:10:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__35"

	// $ANTLR start "T__36"
	public final void mT__36() throws RecognitionException {
		try {
			int _type = T__36;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:11:7: ( '-' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:11:9: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__36"

	// $ANTLR start "T__37"
	public final void mT__37() throws RecognitionException {
		try {
			int _type = T__37;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:12:7: ( '/' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:12:9: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__37"

	// $ANTLR start "T__38"
	public final void mT__38() throws RecognitionException {
		try {
			int _type = T__38;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:13:7: ( ';' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:13:9: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__38"

	// $ANTLR start "T__39"
	public final void mT__39() throws RecognitionException {
		try {
			int _type = T__39;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:14:7: ( '=' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:14:9: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__39"

	// $ANTLR start "T__40"
	public final void mT__40() throws RecognitionException {
		try {
			int _type = T__40;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:15:7: ( '^' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:15:9: '^'
			{
			match('^'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__40"

	// $ANTLR start "FUNCTION"
	public final void mFUNCTION() throws RecognitionException {
		try {
			int _type = FUNCTION;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:245:2: ( 'function' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:245:4: 'function'
			{
			match("function"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FUNCTION"

	// $ANTLR start "RETURN"
	public final void mRETURN() throws RecognitionException {
		try {
			int _type = RETURN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:246:8: ( 'return' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:246:10: 'return'
			{
			match("return"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RETURN"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:247:4: ( 'if' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:247:6: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			int _type = THEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:248:6: ( 'then' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:248:8: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "THEN"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:249:6: ( 'else' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:249:8: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "WHILE"
	public final void mWHILE() throws RecognitionException {
		try {
			int _type = WHILE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:250:7: ( 'while' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:250:9: 'while'
			{
			match("while"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHILE"

	// $ANTLR start "FOR"
	public final void mFOR() throws RecognitionException {
		try {
			int _type = FOR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:251:5: ( 'for' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:251:7: 'for'
			{
			match("for"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FOR"

	// $ANTLR start "DO"
	public final void mDO() throws RecognitionException {
		try {
			int _type = DO;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:252:4: ( 'do' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:252:6: 'do'
			{
			match("do"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DO"

	// $ANTLR start "END"
	public final void mEND() throws RecognitionException {
		try {
			int _type = END;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:253:5: ( 'end' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:253:7: 'end'
			{
			match("end"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "END"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:254:5: ( 'div' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:254:7: 'div'
			{
			match("div"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "MOD"
	public final void mMOD() throws RecognitionException {
		try {
			int _type = MOD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:255:5: ( '%' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:255:7: '%'
			{
			match('%'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MOD"

	// $ANTLR start "EQ"
	public final void mEQ() throws RecognitionException {
		try {
			int _type = EQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:256:4: ( '==' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:256:6: '=='
			{
			match("=="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQ"

	// $ANTLR start "NEQ"
	public final void mNEQ() throws RecognitionException {
		try {
			int _type = NEQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:257:5: ( '!=' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:257:7: '!='
			{
			match("!="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEQ"

	// $ANTLR start "GT"
	public final void mGT() throws RecognitionException {
		try {
			int _type = GT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:258:4: ( '>' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:258:6: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GT"

	// $ANTLR start "GEQ"
	public final void mGEQ() throws RecognitionException {
		try {
			int _type = GEQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:259:5: ( '>=' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:259:7: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GEQ"

	// $ANTLR start "LT"
	public final void mLT() throws RecognitionException {
		try {
			int _type = LT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:260:4: ( '<' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:260:6: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LT"

	// $ANTLR start "LEQ"
	public final void mLEQ() throws RecognitionException {
		try {
			int _type = LEQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:261:5: ( '<=' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:261:7: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LEQ"

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:262:5: ( 'and' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:262:7: 'and'
			{
			match("and"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:263:4: ( 'or' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:263:6: 'or'
			{
			match("or"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "NOT"
	public final void mNOT() throws RecognitionException {
		try {
			int _type = NOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:264:5: ( 'not' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:264:7: 'not'
			{
			match("not"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:266:5: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:266:7: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:266:31: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:269:5: ( ( '0' .. '9' )+ )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:269:7: ( '0' .. '9' )+
			{
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:269:7: ( '0' .. '9' )+
			int cnt2=0;
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt2 >= 1 ) break loop2;
					EarlyExitException eee = new EarlyExitException(2, input);
					throw eee;
				}
				cnt2++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "FLOAT"
	public final void mFLOAT() throws RecognitionException {
		try {
			int _type = FLOAT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:273:5: ( ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( EXPONENT )? | '.' ( '0' .. '9' )+ ( EXPONENT )? | ( '0' .. '9' )+ EXPONENT )
			int alt9=3;
			alt9 = dfa9.predict(input);
			switch (alt9) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:273:9: ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( EXPONENT )?
					{
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:273:9: ( '0' .. '9' )+
					int cnt3=0;
					loop3:
					while (true) {
						int alt3=2;
						int LA3_0 = input.LA(1);
						if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
							alt3=1;
						}

						switch (alt3) {
						case 1 :
							// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt3 >= 1 ) break loop3;
							EarlyExitException eee = new EarlyExitException(3, input);
							throw eee;
						}
						cnt3++;
					}

					match('.'); 
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:273:25: ( '0' .. '9' )*
					loop4:
					while (true) {
						int alt4=2;
						int LA4_0 = input.LA(1);
						if ( ((LA4_0 >= '0' && LA4_0 <= '9')) ) {
							alt4=1;
						}

						switch (alt4) {
						case 1 :
							// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop4;
						}
					}

					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:273:37: ( EXPONENT )?
					int alt5=2;
					int LA5_0 = input.LA(1);
					if ( (LA5_0=='E'||LA5_0=='e') ) {
						alt5=1;
					}
					switch (alt5) {
						case 1 :
							// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:273:37: EXPONENT
							{
							mEXPONENT(); 

							}
							break;

					}

					}
					break;
				case 2 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:274:9: '.' ( '0' .. '9' )+ ( EXPONENT )?
					{
					match('.'); 
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:274:13: ( '0' .. '9' )+
					int cnt6=0;
					loop6:
					while (true) {
						int alt6=2;
						int LA6_0 = input.LA(1);
						if ( ((LA6_0 >= '0' && LA6_0 <= '9')) ) {
							alt6=1;
						}

						switch (alt6) {
						case 1 :
							// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt6 >= 1 ) break loop6;
							EarlyExitException eee = new EarlyExitException(6, input);
							throw eee;
						}
						cnt6++;
					}

					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:274:25: ( EXPONENT )?
					int alt7=2;
					int LA7_0 = input.LA(1);
					if ( (LA7_0=='E'||LA7_0=='e') ) {
						alt7=1;
					}
					switch (alt7) {
						case 1 :
							// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:274:25: EXPONENT
							{
							mEXPONENT(); 

							}
							break;

					}

					}
					break;
				case 3 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:275:9: ( '0' .. '9' )+ EXPONENT
					{
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:275:9: ( '0' .. '9' )+
					int cnt8=0;
					loop8:
					while (true) {
						int alt8=2;
						int LA8_0 = input.LA(1);
						if ( ((LA8_0 >= '0' && LA8_0 <= '9')) ) {
							alt8=1;
						}

						switch (alt8) {
						case 1 :
							// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt8 >= 1 ) break loop8;
							EarlyExitException eee = new EarlyExitException(8, input);
							throw eee;
						}
						cnt8++;
					}

					mEXPONENT(); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FLOAT"

	// $ANTLR start "STRING"
	public final void mSTRING() throws RecognitionException {
		try {
			int _type = STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:279:5: ( '\"' (~ ( '\\\\' | '\"' ) )* '\"' )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:279:8: '\"' (~ ( '\\\\' | '\"' ) )* '\"'
			{
			match('\"'); 
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:279:12: (~ ( '\\\\' | '\"' ) )*
			loop10:
			while (true) {
				int alt10=2;
				int LA10_0 = input.LA(1);
				if ( ((LA10_0 >= '\u0000' && LA10_0 <= '!')||(LA10_0 >= '#' && LA10_0 <= '[')||(LA10_0 >= ']' && LA10_0 <= '\uFFFF')) ) {
					alt10=1;
				}

				switch (alt10) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop10;
				}
			}

			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:283:5: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' | '/*' ( options {greedy=false; } : . )* '*/' )
			int alt14=2;
			int LA14_0 = input.LA(1);
			if ( (LA14_0=='/') ) {
				int LA14_1 = input.LA(2);
				if ( (LA14_1=='/') ) {
					alt14=1;
				}
				else if ( (LA14_1=='*') ) {
					alt14=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 14, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 14, 0, input);
				throw nvae;
			}

			switch (alt14) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:283:9: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
					{
					match("//"); 

					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:283:14: (~ ( '\\n' | '\\r' ) )*
					loop11:
					while (true) {
						int alt11=2;
						int LA11_0 = input.LA(1);
						if ( ((LA11_0 >= '\u0000' && LA11_0 <= '\t')||(LA11_0 >= '\u000B' && LA11_0 <= '\f')||(LA11_0 >= '\u000E' && LA11_0 <= '\uFFFF')) ) {
							alt11=1;
						}

						switch (alt11) {
						case 1 :
							// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:
							{
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop11;
						}
					}

					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:283:28: ( '\\r' )?
					int alt12=2;
					int LA12_0 = input.LA(1);
					if ( (LA12_0=='\r') ) {
						alt12=1;
					}
					switch (alt12) {
						case 1 :
							// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:283:28: '\\r'
							{
							match('\r'); 
							}
							break;

					}

					match('\n'); 
					_channel=HIDDEN;
					}
					break;
				case 2 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:284:9: '/*' ( options {greedy=false; } : . )* '*/'
					{
					match("/*"); 

					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:284:14: ( options {greedy=false; } : . )*
					loop13:
					while (true) {
						int alt13=2;
						int LA13_0 = input.LA(1);
						if ( (LA13_0=='*') ) {
							int LA13_1 = input.LA(2);
							if ( (LA13_1=='/') ) {
								alt13=2;
							}
							else if ( ((LA13_1 >= '\u0000' && LA13_1 <= '.')||(LA13_1 >= '0' && LA13_1 <= '\uFFFF')) ) {
								alt13=1;
							}

						}
						else if ( ((LA13_0 >= '\u0000' && LA13_0 <= ')')||(LA13_0 >= '+' && LA13_0 <= '\uFFFF')) ) {
							alt13=1;
						}

						switch (alt13) {
						case 1 :
							// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:284:42: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop13;
						}
					}

					match("*/"); 

					_channel=HIDDEN;
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:287:5: ( ( ' ' | '\\r' | '\\n' | '\\t' ) )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:287:9: ( ' ' | '\\r' | '\\n' | '\\t' )
			{
			if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "EXPONENT"
	public final void mEXPONENT() throws RecognitionException {
		try {
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:300:10: ( ( 'e' | 'E' ) ( '+' | '-' )? ( '0' .. '9' )+ )
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:300:12: ( 'e' | 'E' ) ( '+' | '-' )? ( '0' .. '9' )+
			{
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:300:22: ( '+' | '-' )?
			int alt15=2;
			int LA15_0 = input.LA(1);
			if ( (LA15_0=='+'||LA15_0=='-') ) {
				alt15=1;
			}
			switch (alt15) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:
					{
					if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:300:33: ( '0' .. '9' )+
			int cnt16=0;
			loop16:
			while (true) {
				int alt16=2;
				int LA16_0 = input.LA(1);
				if ( ((LA16_0 >= '0' && LA16_0 <= '9')) ) {
					alt16=1;
				}

				switch (alt16) {
				case 1 :
					// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt16 >= 1 ) break loop16;
					EarlyExitException eee = new EarlyExitException(16, input);
					throw eee;
				}
				cnt16++;
			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXPONENT"

	@Override
	public void mTokens() throws RecognitionException {
		// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:8: ( T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | FUNCTION | RETURN | IF | THEN | ELSE | WHILE | FOR | DO | END | DIV | MOD | EQ | NEQ | GT | GEQ | LT | LEQ | AND | OR | NOT | ID | INT | FLOAT | STRING | COMMENT | WS )
		int alt17=36;
		alt17 = dfa17.predict(input);
		switch (alt17) {
			case 1 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:10: T__31
				{
				mT__31(); 

				}
				break;
			case 2 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:16: T__32
				{
				mT__32(); 

				}
				break;
			case 3 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:22: T__33
				{
				mT__33(); 

				}
				break;
			case 4 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:28: T__34
				{
				mT__34(); 

				}
				break;
			case 5 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:34: T__35
				{
				mT__35(); 

				}
				break;
			case 6 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:40: T__36
				{
				mT__36(); 

				}
				break;
			case 7 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:46: T__37
				{
				mT__37(); 

				}
				break;
			case 8 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:52: T__38
				{
				mT__38(); 

				}
				break;
			case 9 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:58: T__39
				{
				mT__39(); 

				}
				break;
			case 10 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:64: T__40
				{
				mT__40(); 

				}
				break;
			case 11 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:70: FUNCTION
				{
				mFUNCTION(); 

				}
				break;
			case 12 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:79: RETURN
				{
				mRETURN(); 

				}
				break;
			case 13 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:86: IF
				{
				mIF(); 

				}
				break;
			case 14 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:89: THEN
				{
				mTHEN(); 

				}
				break;
			case 15 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:94: ELSE
				{
				mELSE(); 

				}
				break;
			case 16 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:99: WHILE
				{
				mWHILE(); 

				}
				break;
			case 17 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:105: FOR
				{
				mFOR(); 

				}
				break;
			case 18 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:109: DO
				{
				mDO(); 

				}
				break;
			case 19 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:112: END
				{
				mEND(); 

				}
				break;
			case 20 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:116: DIV
				{
				mDIV(); 

				}
				break;
			case 21 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:120: MOD
				{
				mMOD(); 

				}
				break;
			case 22 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:124: EQ
				{
				mEQ(); 

				}
				break;
			case 23 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:127: NEQ
				{
				mNEQ(); 

				}
				break;
			case 24 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:131: GT
				{
				mGT(); 

				}
				break;
			case 25 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:134: GEQ
				{
				mGEQ(); 

				}
				break;
			case 26 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:138: LT
				{
				mLT(); 

				}
				break;
			case 27 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:141: LEQ
				{
				mLEQ(); 

				}
				break;
			case 28 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:145: AND
				{
				mAND(); 

				}
				break;
			case 29 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:149: OR
				{
				mOR(); 

				}
				break;
			case 30 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:152: NOT
				{
				mNOT(); 

				}
				break;
			case 31 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:156: ID
				{
				mID(); 

				}
				break;
			case 32 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:159: INT
				{
				mINT(); 

				}
				break;
			case 33 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:163: FLOAT
				{
				mFLOAT(); 

				}
				break;
			case 34 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:169: STRING
				{
				mSTRING(); 

				}
				break;
			case 35 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:176: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 36 :
				// C:\\Users\\Vagner\\Progs\\Java\\LFA\\LUA\\grammars\\lua.g:1:184: WS
				{
				mWS(); 

				}
				break;

		}
	}


	protected DFA9 dfa9 = new DFA9(this);
	protected DFA17 dfa17 = new DFA17(this);
	static final String DFA9_eotS =
		"\5\uffff";
	static final String DFA9_eofS =
		"\5\uffff";
	static final String DFA9_minS =
		"\2\56\3\uffff";
	static final String DFA9_maxS =
		"\1\71\1\145\3\uffff";
	static final String DFA9_acceptS =
		"\2\uffff\1\2\1\1\1\3";
	static final String DFA9_specialS =
		"\5\uffff}>";
	static final String[] DFA9_transitionS = {
			"\1\2\1\uffff\12\1",
			"\1\3\1\uffff\12\1\13\uffff\1\4\37\uffff\1\4",
			"",
			"",
			""
	};

	static final short[] DFA9_eot = DFA.unpackEncodedString(DFA9_eotS);
	static final short[] DFA9_eof = DFA.unpackEncodedString(DFA9_eofS);
	static final char[] DFA9_min = DFA.unpackEncodedStringToUnsignedChars(DFA9_minS);
	static final char[] DFA9_max = DFA.unpackEncodedStringToUnsignedChars(DFA9_maxS);
	static final short[] DFA9_accept = DFA.unpackEncodedString(DFA9_acceptS);
	static final short[] DFA9_special = DFA.unpackEncodedString(DFA9_specialS);
	static final short[][] DFA9_transition;

	static {
		int numStates = DFA9_transitionS.length;
		DFA9_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA9_transition[i] = DFA.unpackEncodedString(DFA9_transitionS[i]);
		}
	}

	protected class DFA9 extends DFA {

		public DFA9(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 9;
			this.eot = DFA9_eot;
			this.eof = DFA9_eof;
			this.min = DFA9_min;
			this.max = DFA9_max;
			this.accept = DFA9_accept;
			this.special = DFA9_special;
			this.transition = DFA9_transition;
		}
		@Override
		public String getDescription() {
			return "272:1: FLOAT : ( ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( EXPONENT )? | '.' ( '0' .. '9' )+ ( EXPONENT )? | ( '0' .. '9' )+ EXPONENT );";
		}
	}

	static final String DFA17_eotS =
		"\7\uffff\1\37\1\uffff\1\41\1\uffff\7\31\2\uffff\1\55\1\57\3\31\1\uffff"+
		"\1\63\7\uffff\3\31\1\67\4\31\1\74\1\31\4\uffff\1\31\1\77\1\31\1\uffff"+
		"\1\31\1\102\1\31\1\uffff\2\31\1\106\1\31\1\uffff\1\110\1\111\1\uffff\1"+
		"\112\1\31\1\uffff\1\31\1\115\1\116\1\uffff\1\31\3\uffff\2\31\2\uffff\1"+
		"\122\1\31\1\124\1\uffff\1\31\1\uffff\1\126\1\uffff";
	static final String DFA17_eofS =
		"\127\uffff";
	static final String DFA17_minS =
		"\1\11\6\uffff\1\52\1\uffff\1\75\1\uffff\1\157\1\145\1\146\1\150\1\154"+
		"\1\150\1\151\2\uffff\2\75\1\156\1\162\1\157\1\uffff\1\56\7\uffff\1\156"+
		"\1\162\1\164\1\60\1\145\1\163\1\144\1\151\1\60\1\166\4\uffff\1\144\1\60"+
		"\1\164\1\uffff\1\143\1\60\1\165\1\uffff\1\156\1\145\1\60\1\154\1\uffff"+
		"\2\60\1\uffff\1\60\1\164\1\uffff\1\162\2\60\1\uffff\1\145\3\uffff\1\151"+
		"\1\156\2\uffff\1\60\1\157\1\60\1\uffff\1\156\1\uffff\1\60\1\uffff";
	static final String DFA17_maxS =
		"\1\172\6\uffff\1\57\1\uffff\1\75\1\uffff\1\165\1\145\1\146\1\150\1\156"+
		"\1\150\1\157\2\uffff\2\75\1\156\1\162\1\157\1\uffff\1\145\7\uffff\1\156"+
		"\1\162\1\164\1\172\1\145\1\163\1\144\1\151\1\172\1\166\4\uffff\1\144\1"+
		"\172\1\164\1\uffff\1\143\1\172\1\165\1\uffff\1\156\1\145\1\172\1\154\1"+
		"\uffff\2\172\1\uffff\1\172\1\164\1\uffff\1\162\2\172\1\uffff\1\145\3\uffff"+
		"\1\151\1\156\2\uffff\1\172\1\157\1\172\1\uffff\1\156\1\uffff\1\172\1\uffff";
	static final String DFA17_acceptS =
		"\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\uffff\1\10\1\uffff\1\12\7\uffff\1\25"+
		"\1\27\5\uffff\1\37\1\uffff\1\41\1\42\1\44\1\43\1\7\1\26\1\11\12\uffff"+
		"\1\31\1\30\1\33\1\32\3\uffff\1\40\3\uffff\1\15\4\uffff\1\22\2\uffff\1"+
		"\35\2\uffff\1\21\3\uffff\1\23\1\uffff\1\24\1\34\1\36\2\uffff\1\16\1\17"+
		"\3\uffff\1\20\1\uffff\1\14\1\uffff\1\13";
	static final String DFA17_specialS =
		"\127\uffff}>";
	static final String[] DFA17_transitionS = {
			"\2\35\2\uffff\1\35\22\uffff\1\35\1\23\1\34\2\uffff\1\22\2\uffff\1\1\1"+
			"\2\1\3\1\4\1\5\1\6\1\33\1\7\12\32\1\uffff\1\10\1\25\1\11\1\24\2\uffff"+
			"\32\31\3\uffff\1\12\1\31\1\uffff\1\26\2\31\1\21\1\17\1\13\2\31\1\15\4"+
			"\31\1\30\1\27\2\31\1\14\1\31\1\16\2\31\1\20\3\31",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\36\4\uffff\1\36",
			"",
			"\1\40",
			"",
			"\1\43\5\uffff\1\42",
			"\1\44",
			"\1\45",
			"\1\46",
			"\1\47\1\uffff\1\50",
			"\1\51",
			"\1\53\5\uffff\1\52",
			"",
			"",
			"\1\54",
			"\1\56",
			"\1\60",
			"\1\61",
			"\1\62",
			"",
			"\1\33\1\uffff\12\32\13\uffff\1\33\37\uffff\1\33",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\64",
			"\1\65",
			"\1\66",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			"\1\70",
			"\1\71",
			"\1\72",
			"\1\73",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			"\1\75",
			"",
			"",
			"",
			"",
			"\1\76",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			"\1\100",
			"",
			"\1\101",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			"\1\103",
			"",
			"\1\104",
			"\1\105",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			"\1\107",
			"",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			"",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			"\1\113",
			"",
			"\1\114",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			"",
			"\1\117",
			"",
			"",
			"",
			"\1\120",
			"\1\121",
			"",
			"",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			"\1\123",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			"",
			"\1\125",
			"",
			"\12\31\7\uffff\32\31\4\uffff\1\31\1\uffff\32\31",
			""
	};

	static final short[] DFA17_eot = DFA.unpackEncodedString(DFA17_eotS);
	static final short[] DFA17_eof = DFA.unpackEncodedString(DFA17_eofS);
	static final char[] DFA17_min = DFA.unpackEncodedStringToUnsignedChars(DFA17_minS);
	static final char[] DFA17_max = DFA.unpackEncodedStringToUnsignedChars(DFA17_maxS);
	static final short[] DFA17_accept = DFA.unpackEncodedString(DFA17_acceptS);
	static final short[] DFA17_special = DFA.unpackEncodedString(DFA17_specialS);
	static final short[][] DFA17_transition;

	static {
		int numStates = DFA17_transitionS.length;
		DFA17_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA17_transition[i] = DFA.unpackEncodedString(DFA17_transitionS[i]);
		}
	}

	protected class DFA17 extends DFA {

		public DFA17(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 17;
			this.eot = DFA17_eot;
			this.eof = DFA17_eof;
			this.min = DFA17_min;
			this.max = DFA17_max;
			this.accept = DFA17_accept;
			this.special = DFA17_special;
			this.transition = DFA17_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | FUNCTION | RETURN | IF | THEN | ELSE | WHILE | FOR | DO | END | DIV | MOD | EQ | NEQ | GT | GEQ | LT | LEQ | AND | OR | NOT | ID | INT | FLOAT | STRING | COMMENT | WS );";
		}
	}

}
